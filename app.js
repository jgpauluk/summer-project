var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
//var multer = require('multer');
var urlencode = require('urlencode');
//var json = require('json-middleware');
//var multipart = require('connect-multiparty');
//var multipartMiddleware = multipart();

var routes = require('./server/routes/index');

app = express();

app.use(express.static(path.join(__dirname, './client')));

app.use(cookieParser());
app.use(expressSession({
	secret: '123456',
	resave: false,
	saveUninitialized: true
}));

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//app.use(multer()); // for parsing multipart/form-data
//app.use(express.json());       // to support JSON-encoded bodies
//app.use(express.urlencoded()); // to support URL-encoded bodies
//app.use('/url/that/accepts/form-data', multipartMiddleware);

//app.listen(8080);

app.use('/', routes);

module.exports = app;
