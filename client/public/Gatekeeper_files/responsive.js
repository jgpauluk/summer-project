(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'bootstrap'], factory);
    } else {
        // Browser globals
        root.socs = root.socs || {};
        root.socs.page = root.socs.page || {};
        root.socs.page.responsive = factory(root.jQuery);
    }
}(this, function ($) {

    $(function() {
        $('[data-toggle=offcanvas]').click(function() {
            $('.row-offcanvas').toggleClass('active');
        });
    });

    return {};

}));
