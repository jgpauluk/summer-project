// $Id: hompageFeatureControl.js 2501 2013-11-06 11:01:34Z nixonm $

;(function($j) {

    stopRotate = false;
	processingFeatureChange = false;

	function featureNext() {

		if (!processingFeatureChange){
			
			processingFeatureChange = true;
		
			$j('#features ul li.last a').unbind('click',featureNext);
					
			if ($jfeatures.length >1){
				$jcurrent = $jfeatures.eq(0);
				$jnext = $jcurrent.next();
				$jcurrent.find('h2').fadeOut('slow');
				$jnext.find('h2').fadeIn('slow');
				$jnext.fadeIn('slow',function(){
					$jcurrent.insertAfter($jfeatures.eq($jfeatures.length-1));
					$jcurrent.hide();
					$jfeatures = $j('#features span.sys_featureImage');
					$j('#features ul li.last a').bind('click',featureNext);
					processingFeatureChange = false;
				});
			}

		}
		return false;
	}

	function featurePrev() {

		if (!processingFeatureChange){
		
			processingFeatureChange = true;
			
			$j('#features ul li.first a').unbind('click',featurePrev);
			
			if ($jfeatures.length > 1){
			
				$jcurrent = $jfeatures.eq(0);
				$jnext = $jfeatures.eq($jfeatures.length-1);
				$jcurrent.find('h2').fadeOut('slow');
				$jnext.find('h2').fadeIn('slow');
				$jnext.fadeIn('slow',function(){
					$jnext.insertBefore($jcurrent);
					$jcurrent.hide();
					$jfeatures = $j('#features span.sys_featureImage');
					$j('#features ul li.first a').bind('click',featurePrev);
					processingFeatureChange = false;

				});
				
			}

		}

		return false;
	}

	$j(document).ready(function(){

		$jfeaturesDiv = $j('#features');
		
		//Add navigation buttons with jQuery as they're no use without javascript
		//but only if there's more than one image
		if ($j("#features>span").length > 1){
			$j('#features').append(
				'<ul> 			<li class="sys_first"><a href="javascript:void(0)">Left</a><li> 			<li class="sys_last"><a 				href="javascript:void(0)">Right</a></li> 		</ul>');

			//Setup feature rotation
			$jfeaturesDiv.find('ul li.sys_last a').bind('click',featureNext);
			$jfeaturesDiv.find('ul li.sys_first a').bind('click',featurePrev);
			$jfeaturesDiv.mouseenter(function(){ //stop rotating images when mouse over feature area
				stopRotate=true;
			});
			$jfeaturesDiv.mouseleave(function(){
				stopRotate=false;
			});
			$jfeatures = $j('#features span.sys_featureImage');
						
			//Setup the timer for feature rotation
			setTimeout(function() {
				rotateFeature();
			}, 8000);

		}
		

	});

	function rotateFeature(){
		if(!stopRotate)
			featureNext();
		
		setTimeout(function() {
				rotateFeature();
			}, 8000);
	}


})(jQuery);