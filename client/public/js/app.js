angular.module('SoCS', ['ngRoute', 'ngCookies'])

	.controller('userList', function($scope, $http, AuthenticationService){
		$scope.userList = {};
		
		$http.get('/user')
			.success(function(data){
				$scope.userList = data;
				console.log(data);
			})
		    .error(function(error) {
				if(error.code === 122){
					AuthenticationService.ClearCredentials();
					$location.path('/gatekeeper');					
				}
				console.log("Error" + error);
			});
	})
	.controller('subjectTaught', function($scope, $http, $location, AuthenticationService){		
		$scope.seeStudents = function(id){
			console.log(id);
			$location.path('/enroll/list/'+id);
		};
		
		$scope.subjectList = {};
		
		$http.get('/subject/' + '0') //user_id
			.success(function(data){
				$scope.subjectList = data;
				console.log(data);
			})
		    .error(function(error) {
				if(error.code === 122){
					AuthenticationService.ClearCredentials();
					$location.path('/gatekeeper');					
				}
				console.log("Error" + error);
			});

		$scope.logout = function(){
			AuthenticationService.ClearCredentials();
			$location.path('/gatekeeper');
			$http.get('/logout')
				.success(function(data){
					//AuthenticationService.ClearCredentials();
					$location.path('/gatekeeper');
				})
				.error(function(error) {
					if(error.code === 122){
						$location.path('/gatekeeper');					
					}
					console.log("Error" + error);
				});
		};
	})
	.controller('enrollCtl', function($scope, $http, $routeParams, $location, AuthenticationService){
		$scope.enrolls = {};

		var id = $routeParams.id;
		
		$http.get('/enroll/' + id) //subjectId
			.success(function(data){
				$scope.enrolls = data;
				console.log(data);
			})
		    .error(function(error) {
				if(error.code === 122){
					AuthenticationService.ClearCredentials();
					$location.path('/gatekeeper');					
				}
				console.log("Error " + error.mesg);
			});

		$scope.logout = function(){
			$http.get('/logout')
				.success(function(data){
					//AuthenticationService.ClearCredentials();
					$location.path('/gatekeeper');
				})
				.error(function(error) {
					if(error.code === 122){
						$location.path('/gatekeeper');					
					}
					console.log("Error" + error);
				});
		};
	})

	.controller('loginCtl', function($scope, $http, $location, AuthenticationService, $anchorScroll){
		var vm = this;

		vm.login = login;

		$scope.incorrentCredentials = false;
		
		(function initController(){
			AuthenticationService.ClearCredentials();
		})();

		function login() {
			vm.dataLoading = true;
			AuthenticationService.Login(vm.username, vm.password, function(response) {
				if(response.success) {
					AuthenticationService.SetCredentials(vm.username, vm.password);
					console.log("Success Login");
					vm.name = response.name;
					$location.path('/taught');
				} else{
					console.log(response.message);
					$scope.incorretCredentials = true;
					$location.hash('inc_cred');
					$anchorScroll();
					vm.dataLoading = false;
				}
			});
		};
		
		$scope.sendCred = function(){
			var cred = {user: $scope.user, password: $scope.password};
			$http.post('/sendAuth', cred)
			    .success(function(data){
					$scope.success = data;
					console.log(data);
					if (data == 1)
						$location.path('/taught');
					else
						console.log("login error");
				})
				.error(function(error) {
					console.log("Error" + error);
				});
		};

		
	})

	.config(function($routeProvider, $locationProvider) {
		var viewPath = '../../views/';
		
		$routeProvider
			.when('/gatekeeper', {
				templateUrl: 'views/gatekeeper.html',
				controller: 'loginCtl',
				controllerAs: 'vm'
			})
			.when('/taught', {
				templateUrl: 'views/subjects_taught.html',
				controller: 'subjectTaught',
				controllerAs: 'vm'
			})
			.when('/enroll/list/:id', {
				templateUrl: 'views/enroll.html',
				controller: 'enrollCtl',
				controllerAs: 'vm'
			})
		    .otherwise({
				redirectTo: '/gatekeeper'
			});
	})
	.run(function($rootScope, $location, $cookieStore, $http) {
		$rootScope.globals = $cookieStore.get('globals') || {};
		if ($rootScope.globals.currentUser) {
			$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
			
		}

		$rootScope.$on('$locationChangeStart', function(event, next, current){
			var restrictedPage = $.inArray($location.path(), ['/gatekeeper']) === -1;
			var loggedIn = $rootScope.globals.currentUser;
			if (restrictedPage && !loggedIn){
				//$location.path('/gatekeeper');
			}
		});
	});
				
