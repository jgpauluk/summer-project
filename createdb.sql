CREATE TABLE users (
	id serial PRIMARY KEY,
	username varchar(255) UNIQUE NOT NULL,
	passwd varchar(32) NOT NULL,
	name varchar(255) 
);

CREATE TABLE students (
	id serial PRIMARY KEY,
	first_name varchar(255),
	last_name varchar(255),
	email varchar(255)
);

CREATE TABLE subjects (
	id serial PRIMARY KEY,
	subject_name varchar(255),
	user_id integer REFERENCES users
);

CREATE TABLE enrollments (
	id serial PRIMARY KEY,
	subject_id integer REFERENCES subjects,
	student_id integer REFERENCES students,
	grades real
);



