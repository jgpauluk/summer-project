CREATE DATABASE unibham;
GRANT CONNECT ON DATABASE chuchu TO joao;


CREATE TABLE users (
	id SERIAL NOT NULL PRIMARY KEY,
	username varchar(255) NOT NULL,
	passwd varchar(32) NOT NULL,
	name varchar(255)
);

CREATE TABLE students (
	id SERIAL NOT NULL PRIMARY KEY,
	first_name varchar(255),
	last_name varchar(255),
	email varchar(255)
);

CREATE TABLE subjects (
	id SERIAL NOT NULL PRIMARY KEY,
	subject_name varchar(255),
	user_id integer REFERENCES users(id)
);
	
CREATE TABLE enrollments (
	id SERIAL NOT NULL PRIMARY KEY,
	subject_id integer REFERENCES subjects(id),
	student_id integer REFERENCES students(id),
	grades real
);
