var express = require('express');
var router = express.Router();
var path = require('path');
var pg = require('pg');
var connectionString = require(path.join(__dirname, '../', '../', 'config'));

router.get('/', function(req, res, next){
	res.sendFile(path.join(__dirname, '../', '../', 'client', 'views', 'index.html'));
});/*

router.get('/enroll', function(req, res, next){
	res.sendFile(path.join(__dirname, '../', '../', 'client', 'views', 'subjects_taught.html'));
});/*

router.get('/enroll', function(req, res, next){
	res.sendFile(path.join(__dirname, '../', '../', 'client', 'views', 'enroll.html'));
}); */

router.get('/user', function(req, res){
	var results = [];

	if (typeof(req.session.userId) === 'undefined'){
		res.clearCookie("globals");
		return res.status(500).send({code: 122, mesg: "session expired"}); //122: not logged in	
	}
	
	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT * FROM users;");

		query.on('row', function(row) {
			results.push(row);
		});

		query.on('end', function(){
			client.end();
			return res.json(results);
		});
	});
});

router.get('/subject/:userId', function(req, res){
	var results = [];

	var id = req.params.userId;

	if (typeof(req.session.userId) === 'undefined'){
		/*res.clearCookie("globals");
		  return res.status(500).send({code: 122, mesg: "session expired"}); //122: not logged in	*/
		req.session.userId = 1;
	}
	
	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT subjects.id, subjects.subject_name, users.username FROM subjects, users WHERE users.id = $1 AND users.id=subjects.user_id;", [req.session.userId]);

		query.on('row', function(row) {
			results.push(row);
		});

		query.on('end', function(){
			client.end();
			return res.json(results);
		});
	});
});

router.get('/logout', function(req, res){
	return req.session.destroy();
});

router.get('/enroll/:subjId', function(req, res){
	var results = [];

	var subject_id = req.params.subjId;
	
	if (typeof(req.session.userId) === 'undefined'){
		/*res.clearCookie("globals");
		  return res.status(500).send({code: 122, mesg: "session expired"}); //122: not logged in	*/
		req.session.userId = 1;
	}
	
	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT enrollments.id, subjects.subject_name, students.first_name, enrollments.grades FROM enrollments, subjects, students WHERE enrollments.subject_id = subjects.id AND enrollments.student_id = students.id AND subjects.user_id=$1 AND subjects.id = $2;", [req.session.userId, subject_id]);
		
		query.on('row', function(row) {
			results.push(row);
		});

		query.on('end', function(){
			client.end();
			return res.json(results);
		});
	});
});

router.post('/api/authenticate', function(req, res){
	var results = [];

	var username = req.body.username;
	var	password = req.body.password;
		
	pg.connect(connectionString, function(err, client, done){
		var query = client.query("SELECT * from users WHERE username = $1 AND passwd = $2", [username, password]);
		
		query.on('row', function(row) {
			results.push(row);
		});

		
		query.on('end', function(){
			if (results.length === 1){
				var sess = req.session;
				sess.userId = results[0].id;
				sess.username = results[0].username;

				return res.json({success: true, name: results[0].name});
			}
			else{
				console.log("incorrect user/password");
				return res.json({success: false});
			}
				
		});
		
	});
});

module.exports = router;
